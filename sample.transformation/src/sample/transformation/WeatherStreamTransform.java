package sample.transformation;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.integration.transformer.MessageTransformationException;

public class WeatherStreamTransform {
	public static int PRETTY_PRINT_INDENT_FACTOR = 4;

	public String transform(String payload) {
		try {
			StringBuilder sb = new StringBuilder();
			JSONObject xmlJSONObj = XML.toJSONObject(payload);
			String jsonPrettyPrintString = xmlJSONObj.toString(PRETTY_PRINT_INDENT_FACTOR);
			sb.append(jsonPrettyPrintString);
			return sb.toString();
		} catch (JSONException e) {
			throw new MessageTransformationException(
					"[sample.transformamtion] tell us if it cannot work on this xml: " + e.getMessage(), e);
		}
	}
	
}
