package sample.transformation;


import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

public class Main {
	public static int PRETTY_PRINT_INDENT_FACTOR = 4;
	public static String TEXT_XML_STRING = 
			"<breakfast_menu>\n" +
					"<food>\n" +
						"<name>Belgian Chocolate Waffles</name>\n" +
					"</food>\n" +
			"</breakfast_menu>";
	public static void main(String[] args) {
		try {
			JSONObject xmlJSONObj = XML.toJSONObject(TEXT_XML_STRING);
			String jsonPrettyPrintString = xmlJSONObj.toString(PRETTY_PRINT_INDENT_FACTOR);
			System.out.println(jsonPrettyPrintString);
		} catch (JSONException je) {
			System.out.println(je.toString());
		}
	}
	
}
